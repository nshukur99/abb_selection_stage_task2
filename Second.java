public class Second {
    public static void main(String[] args){
        int[][] A={{5,4,4},{4,3,4} ,{3,2,4} ,{3,2,2},
            {2,2,2},{3,3,4},{1,4,4},{4,1,1}};
          System.out.println(solution(A));
    }
     public static int solution(int[][] A){
         int count=0;
         int n=A.length;
         if(A.length==0)
             return 0;
         int m=A[0].length;
         for(int i=0;i<n;i++){
             for(int j=0;j<m;j++){
                 if(A[i][j]!=Integer.MIN_VALUE){
                     dfs(A,i,j,A[i][j]);
                     count++;
                 }
             }
         }
         return count;
         
     }  
     public static void dfs(int[][] A,int row,int col,int color){
         if(row<0 || col<0 || row>=A.length || col>=A[0].length || A[row][col]!=color)
             return;
         
         A[row][col]=Integer.MIN_VALUE;
         dfs(A,row-1,col,color);
         dfs(A,row,col-1,color);
         dfs(A,row+1,col,color);
         dfs(A,row,col+1,color);
         
     }
}
